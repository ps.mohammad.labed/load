// ignore_for_file: avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/model/db_helper.dart';



import 'widget/build_item.dart';
class Homesc extends StatefulWidget {
  const Homesc({super.key});

  @override
  State<Homesc> createState() => _HomescState();
}


class _HomescState extends State<Homesc> {
  bool _hasmore=true;
  bool _isfeching=false;

fechmore()async{
  // this function checks if there is moredata and fech it
  if(_isfeching){
    return;
  }
  setState(() {
    _isfeching=true;
  });
  try{
    final snap=await fechdata(20,startafter:items.isEmpty?null: items.last);
    items.addAll(snap.docs);
    if(snap.docs.length<20){
      setState(() {
        _hasmore=false;
      });
    }
setState(() {
  _isfeching=false;
});
  }catch(e){
    print(e);
  }
}

  final query=FirebaseFirestore.instance.collection('item').snapshots();
  final ScrollController _scrollctrl=ScrollController();
  @override
  void initState() {
    fechmore();
  _scrollctrl.addListener(() {
    if(_scrollctrl.offset==_scrollctrl.position.maxScrollExtent&&!_scrollctrl.position.outOfRange){
      if(_hasmore){
        fechmore();
      }
     

    }
  });
    super.initState();
  }
  @override
  void dispose() {
   
    super.dispose();
    _scrollctrl.dispose();
  }

List <DocumentSnapshot>items=[];
// the list we store data in
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Stack(
        children:[
          ListView.builder(controller: _scrollctrl,itemBuilder: ((context, index) => Itemwidget(items[index]['item'],
        
          
          
          
          
          )
        
        
        ),itemCount: items.length,),
        _hasmore&&_isfeching?const Positioned(bottom: 20,left: 40,right: 40,child: CircularProgressIndicator()):const SizedBox()
        ] 
      )
    );
  }
}