import 'package:flutter/material.dart';

class Itemwidget extends StatelessWidget {
  final int item;
  //here we get the item and just show it in text

  const Itemwidget(this.item,{super.key});
  

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.arrow_right_rounded),
      title: Text('Item : $item'),
    );
  }
}